Write-Output("@NeotroX")
Write-Output("QR Script by RamiYa")
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
Write-Output("NeotroX Installing...")
scoop update
scoop install nodejs --global
scoop install git

$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
Write-Output("NeotroX Loading...")

git clone https://github.com/xneon2/Hashzi-X
Set-Location Hashzi-X
npm install @adiwajshing/baileys
npm install chalk
node qr.js
